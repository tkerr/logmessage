# LogMessage #
A quick and dirty Arduino message logging class.

Logs timestamped messages to a specified serial port.

The messages include text and one optional data parameter.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
