/*****************************************************************************
 * LogMessage.h
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * A quick and dirty Arduino message logging class.
 */
#ifndef _LOG_MESSAGE_H_
#define _LOG_MESSAGE_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include "Arduino.h"
#include "Print.h"
#include "IPAddress.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "arduino_arch.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @class
 * @brief
 * A quick and dirty Arduino message logging class.
 *
 * Logs a timestamped message and an optional additional numeric value or 
 * string to the serial device specified in the constructor.
 */
class LogMessage
{
public:

    /****
     * The LogMessage object must be declared with a pointer to the serial
     * port object as an argument.
     *
     * Different processor architectures provide different classes for the
     * Serial object.  Constructors are provided for the different classes.
     *
     * Note that the serial device must be initialized before logging can occur.
     * This can be done by calling its begin() method directly, or by calling
     * the LogMessage::begin() method.
     ****/

    LogMessage(HardwareSerial* pSerial); //!< Construct from HardwareSerial and its subclasses
    
    #ifdef ARDUINO_ARCH_SAMD
    LogMessage(Serial_* pSerial); //!< SAMD USB serial device
    #endif  // ARDUINO_ARCH_SAMD
    
    #ifdef ARDUINO_ARCH_TEENSY
    LogMessage(usb_serial_class* pSerial); //!< Teensy USB serial device
    #endif  // ARDUINO_ARCH_TEENSY
    
    /// Enable/disable logging
    void enable(bool enabled = true) {m_enabled = enabled;}
    bool isEnabled() {return m_enabled;}  //!< Return true if enabled, false otherwise
    
    /// Enable/disable timestamps
    void timestamps(bool enabled = true) {m_timestamps = enabled;}
    
    /// Call the Serial object's begin method
    bool begin(unsigned long baud, unsigned int format = SERIAL_8N1);
    
    // Formatted (timestamped) printing.
    void log(const char* msg);
    void log(const char* msg, char val);
    void log(const char* msg, unsigned char val, int base = DEC);
    void log(const char* msg, int val, int base = DEC);
    void log(const char* msg, unsigned int val, int base = DEC);
    void log(const char* msg, long val, int base = DEC);
    void log(const char* msg, unsigned long val, int base = DEC);
    void log(const char* msg, double val, int digits = 2);
    void log(const char* msg, const char* str);
    void log(const char* msg, const char* str, int len);
    void log(const char* msg, const String& str);
    void log(const char* msg, const IPAddress& ip);
    void log(const char* msg, const Printable &obj);
    
    // Unformatted printing.
    
    void print(const String &s) {if (m_enabled) m_pSerial->print(s);}
    void print(char c) {if (m_enabled) m_pSerial->print(c);}
    void print(const char s[]) {if (m_enabled) m_pSerial->print(s);}
    void print(const __FlashStringHelper *f) {if (m_enabled) m_pSerial->print(f);}
    
    void print(unsigned char n, int base = DEC) {if (m_enabled) m_pSerial->print(n, base);}
    void print(int n, int base = DEC) {if (m_enabled) m_pSerial->print(n, base);}
    void print(unsigned int n, int base = DEC) {if (m_enabled) m_pSerial->print(n, base);}
    void print(long n, int base = DEC){if (m_enabled) m_pSerial->print(n, base);}
    void print(unsigned long n, int base = DEC) {if (m_enabled) m_pSerial->print(n, base);}
    
    void print(double n, int digits = 2) {if (m_enabled) m_pSerial->print(n, digits);}
    void print(const Printable &obj) {if (m_enabled) m_pSerial->print(obj);}
    
    void println(void) {if (m_enabled) m_pSerial->println();}
    void println(const String &s) {if (m_enabled) m_pSerial->println(s);}
    void println(char c) {if (m_enabled) m_pSerial->println(c);}
    void println(const char s[]) {if (m_enabled) m_pSerial->println(s);}
    void println(const __FlashStringHelper *f) {if (m_enabled) m_pSerial->println(f);}
    
    void println(unsigned char n, int base = DEC) {if (m_enabled) m_pSerial->println(n, base);}
    void println(int n, int base = DEC) {if (m_enabled) m_pSerial->println(n, base);}
    void println(unsigned int n, int base = DEC) {if (m_enabled) m_pSerial->println(n, base);}
    void println(long n, int base = DEC) {if (m_enabled) m_pSerial->println(n, base);}
    void println(unsigned long n, int base = DEC) {if (m_enabled) m_pSerial->println(n, base);}
    
    void println(double n, int digits = 2) {if (m_enabled) m_pSerial->println(n, digits);}
    void println(const Printable &obj) {if (m_enabled) m_pSerial->println(obj);}
    
protected:
    
private:

    /**
     * @brief Enumerate the serial port class used for this object
     */
    enum SerialClass
    {
        SERIAL_UNKNOWN = 0,   //!< Unknown/unsupported class
        SERIAL_HARDWARE,      //!< HardwareSerial class
        SERIAL_SAMD_USB,      //!< SAMD Serial_ class
        SERIAL_TEENSY_USB,    //!< Teensy usb_serial_class
    };
        
    SerialClass m_serial_class;  //!< The serial port class in use
    Stream*     m_pSerial;       //!< Pointer to the serial port object
    bool        m_enabled;       //!< Enable logging if true
    bool        m_timestamps;    //!< Enable timestamps if true
    
    void printMsg(const char* msg);  //!< Common method to print timestamp and message
};


#endif // _LOG_MESSAGE_H_
