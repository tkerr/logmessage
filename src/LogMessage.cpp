/******************************************************************************
 * LogMessage.cpp
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * A quick and dirty Arduino message logging class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "LogMessage.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * LogMessage::LogMessage
 **************************************/
LogMessage::LogMessage(HardwareSerial* pSerial) :
    m_serial_class(SERIAL_HARDWARE),
    m_enabled(true),
    m_timestamps(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}


#ifdef ARDUINO_ARCH_SAMD
/**************************************
 * LogMessage::LogMessage
 **************************************/
LogMessage::LogMessage(Serial_* pSerial) :
    m_serial_class(SERIAL_SAMD_USB),
    m_enabled(true),
    m_timestamps(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}
#endif // ARDUINO_ARCH_SAMD


#ifdef ARDUINO_ARCH_TEENSY
/**************************************
 * LogMessage::LogMessage
 **************************************/
LogMessage::LogMessage(usb_serial_class* pSerial) :
    m_serial_class(SERIAL_TEENSY_USB),
    m_enabled(true),
    m_timestamps(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}
#endif // ARDUINO_ARCH_TEENSY


/**************************************
 * LogMessage::begin
 **************************************/
bool LogMessage::begin(unsigned long baud, unsigned int format)
{
    bool ok = false;
    
    if (m_pSerial)
    {
        // The begin() method is not part of the Stream class.
        // We must call the begin() method on the original object class.
        switch (m_serial_class)
        {
        case SERIAL_SAMD_USB:
            #ifdef ARDUINO_ARCH_SAMD
            {
                Serial_* p = static_cast<Serial_*>(m_pSerial);
                p->begin(baud, format);
                ok = true;
            }
            #endif
            break;
            
        case SERIAL_TEENSY_USB:
            #ifdef ARDUINO_ARCH_TEENSY
            {
                usb_serial_class* p = static_cast<usb_serial_class*>(m_pSerial);
                p->begin(baud);
                ok = true;
            }
            #endif
            break;
            
        case SERIAL_HARDWARE:
            {
                HardwareSerial* p = static_cast<HardwareSerial*>(m_pSerial);
                p->begin(baud, format);
                ok = true;
            }
            break;
            
        default:
            break;
        }
    }
    return ok;
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->println();
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, char val)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, unsigned char val, int base)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, base);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, int val, int base)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, base);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, unsigned int val, int base)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, base);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, long val, int base)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, base);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, unsigned long val, int base)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, base);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, double val, int digits)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(val, digits);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, const char* str)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(str);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, const String& str)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(str);
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, const char* str, int len)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->write(str, len);
    m_pSerial->println();
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, const IPAddress& ip)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    ip.printTo(*m_pSerial);
    m_pSerial->println();
}


/**************************************
 * LogMessage::log
 **************************************/
void LogMessage::log(const char* msg, const Printable &obj)
{
    if (!m_enabled) return;
    printMsg(msg);
    m_pSerial->print(',');
    m_pSerial->println(obj);
}


/*****************************************************************************
 * Private functions.
 ******************************************************************************/
 
/**************************************
 * LogMessage::printMsg
 **************************************/
void LogMessage::printMsg(const char* msg)
{
    if (m_timestamps) m_pSerial->print(millis());
    m_pSerial->print(',');
    m_pSerial->print(msg);
}

// End of file.
