/******************************************************************************
 * LogMessage_example.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example sketch for LogMessage class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "LogMessage.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define SERIAL_BUF_LEN (128)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
LogMessage myLogMessage(&Serial);
char serialBuf[SERIAL_BUF_LEN];

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Note: random delays don't serve any real purpose.
    // They're just used to provide some variety to the timestamps.
    
    memset(serialBuf, 0, SERIAL_BUF_LEN);
    
    // Must always initialize the underlying serial object.
    if (!myLogMessage.begin(SERIAL_BAUD))
    {
        Serial.end();
        delay(500);
        Serial.begin(SERIAL_BAUD);
        delay(500);
        Serial.println("ERROR: LogMessage.begin() failed.");
        while (true) {}
    }
    
    delay(random(1793, 2999));
    
    Serial.println("LogMessage example sketch");
    myLogMessage.log("Log message with no parameters");
    myLogMessage.log("Character parameter", 'Z');
    Serial.println();
    delay(random(13,29));
    
    unsigned char uc = 0xA5;
    myLogMessage.log("Unsigned char (decimal)", uc);
    myLogMessage.log("Unsigned char (binary)", uc, BIN);
    myLogMessage.log("Unsigned char (octal)", uc, OCT);
    myLogMessage.log("Unsigned char (hex)", uc, HEX);
    Serial.println();
    delay(random(13,29));
    
    int ii = -42405;
    myLogMessage.log("Integer (decimal)", ii);
    myLogMessage.log("Integer (binary)", ii, BIN);
    myLogMessage.log("Integer (octal)", ii, OCT);
    myLogMessage.log("Integer (hex)", ii, HEX);
    Serial.println();
    delay(random(13,29));
    
    unsigned int ui = 42405;
    myLogMessage.log("Unsigned integer (decimal)", ui);
    myLogMessage.log("Unsigned integer (binary)", ui, BIN);
    myLogMessage.log("Unsigned integer (octal)", ui, OCT);
    myLogMessage.log("Unsigned integer (hex)", ui, HEX);
    Serial.println();
    delay(random(13,29));
    
    long li = -7654321;
    myLogMessage.log("Long integer (decimal)", li);
    myLogMessage.log("Long integer (binary)", li, BIN);
    myLogMessage.log("Long integer (octal)", li, OCT);
    myLogMessage.log("Long integer (hex)", li, HEX);
    Serial.println();
    delay(random(13,29));
    
    unsigned long ul = 0xDEADBEEF;
    myLogMessage.log("Unsigned long integer (decimal)", ul);
    myLogMessage.log("Unsigned long integer (binary)", ul, BIN);
    myLogMessage.log("Unsigned long integer (octal)", ul, OCT);
    myLogMessage.log("Unsigned long integer (hex)", ul, HEX);
    Serial.println();
    delay(random(13,29));
    
    myLogMessage.log("Floating point (default digits)", M_PI);
    myLogMessage.log("Floating point (4 digits)", M_PI, 4);
    myLogMessage.log("Floating point (6 digits)", M_PI, 6);
    Serial.println();
    delay(random(13,29));
    
    const char* cstr = "The quick brown fox jumped over the lazy red dog.";
    const String astr = "This is a String object";
    myLogMessage.log("C string", cstr);
    myLogMessage.log("Same C string with length 20", cstr, 20);
    myLogMessage.log("Arduino String object", astr);
    Serial.println();
    delay(random(13,29));
    
    IPAddress ip(192, 168, 101, 102);
    myLogMessage.log("IP address", ip);
    Serial.println();
    
    Serial.println("Logging characters received over the serial port:");
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    int len = Serial.available();
    if (len > SERIAL_BUF_LEN) len = SERIAL_BUF_LEN;
    if (len > 0)
    {
        Serial.readBytes(serialBuf, len);
        myLogMessage.log("Message", serialBuf, len);
    }
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.